# Python Map, Filter와 Reduce 함수

1. <a name="intro"></a> 개요
이 포스팅에서는 Python에서 map, filter와 reduce 함수를 사용하는 방법을 설명한다. 이들은 리스트, 튜플 또는 사전과 같은 반복 가능한 객체에 함수를 적용하여 수정된 요소로 새로운 반복 가능한 함수를 반환할 수 있는 내장된 함수들이다.

함수를 프로그램의 주요 구성 요소로 사용하는 것에 초점을 맞춘 프로그래밍 패러다임인 Python에서 함수형 프로그래밍의 예로는 map, filter와 reduce 함수를 들 수 있다. 함수형 프로그래밍은 테스트와 디버그가 더 쉬운 간결하고 우아하며 재사용 가능한 코드를 작성하는 데 도움을 줄 수 있다.

이 글을 읽고 이해한다면 다음 작업을 수행할 수 있다.

- map, filter와 reduce 함수의 작동 방식과 그에 대한 이해
- map 함수를 사용하여 반복 가능한 각 요소에 함수를 적용하여 새 반복 가능한 요소를 반환한다
- filter 함수를 사용하여 조건을 만족하지 않는 반복 가능한 요소를 필터링하여 새 반복 가능한 요소를 반환한다
- reduce 함수를 사용하여 함수를 반복 가능한 값에 적용하여 단일 값으로 축소 반환한다
- map, filter와 reduce 함수의 성능 비교와 평가

시작합시다!

1. <a name="sec_02"></a> Map, Filter와 Reduce 함수란?
Python에 내장된 세 함수 map, filter와 reduce는 리스트, 튜플 및 사전 같은 반복 가능한 객체에 적용하여 수정된 요소로 새로운 반복 가능한 객체를 반환할 수 있다. 이러한 함수는 함수를 프로그램의 주요 구성 요소로 사용하는 것에 초점을 맞춘 프로그래밍 패러다임인 함수 프로그래밍의 map, filter와 reduce의 개념을 기반으로 한다.

map 함수는 함수와 반복 가능한 두 개의 인수를 취한다. 함수는 반복 가능한 객체의 각 요소에 적용되고 결과와 함께 새로운 반복 가능한 객체를 반환한다. 예를 들어, map 함수를 사용하여 문자열 리스트를 대문자로 변환할 수 있다.

```python
# Define a list of strings
names = ["Alice", "Bob", "Charlie"]
# Define a function to convert a string to uppercase
def to_upper(name):
    return name.upper()
# Use the map function to apply the function to the list
upper_names = map(to_upper, names)
# Convert the map object to a list and print it
print(list(upper_names))
```

출력은 아래와 같다.

```
['ALICE', 'BOB', 'CHARLIE']
```

filter 함수는 또한 함수와 반복가능한 두 개의 인수를 취한다. 그것은 함수를 반복가능한 객체의 각 요소에 적용하여, 그 함수를 만족하는 요소들로만으로 이루어진 새로운 반복가능한 객체를 반환한다. 함수는 각 요소에 대한 부울 값(True 또는 False)을 반환해야 한다. 예를 들어, 필터 함수를 사용하여 정수 리스트에서 홀수를 걸러낼 수 있다.

```python
# Define a list of integers
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]
# Define a function to check if a number is even
def is_even(number):
    return number % 2 == 0
# Use the filter function to apply the function to the list
even_numbers = filter(is_even, numbers)
# Convert the filter object to a list and print it
print(list(even_numbers))
```

출력은 아래와 같다.

```
[2, 4, 6, 8]
```

reduce 함수는 함수와 반복가능한 두 개의 인수를 취한다. 반복가능한 객체의 요소가 소진되어 단일 값이 반환될 때까지 함수를 반복가능한 객체의 처음 두 원소에 적용하고, 결과와 다음 요소에 적용한다. 함수는 두 개의 인수를 사용하여 단일 값을 반환해야 한다. 예를 들어, reduce 함수를 사용하여 리스트에 있는 모든 원소의 곱을 계산할 수 있다.

```python
# Import the reduce function from the functools module
from functools import reduce

# Define a list of integers
numbers = [1, 2, 3, 4, 5]
# Define a function to multiply two numbers
def multiply(x, y):
    return x * y
# Use the reduce function to apply the function to the list
product = reduce(multiply, numbers)
# Print the result
print(product)
```

출력은 아래와 같다.

```
120
```

다음 절에서는 Pythonb에서 map, filter와 reduce 함수를 사용하는 방법과 반복 가능한 객체에 함수를 적용하는 다른 방법과 비교하고 수행하는 방법에 대해 자세히 설명한다.

1. <a name="sec_03"></a> Python에서 map 함수를 사용하는 방법
이 절에서는 Python에서 map 함수를 사용하여 반복 가능한 객체의 각 요소에 함수를 적용하여 그 결과로 새로운 반복 가능한 객체 반환하는 방법을 설명한다. 또한 map 함수를 효과적이고 효율적으로 사용하는 방법에 대한 팁과 요령도 설명할 것이다.

mao 함수는 다음과 같은 구문을 갖는다.

```python
map(function, iterable)
```

함수 인수는 반복 가능한 객체의 각 요소에 적용하고자 하는 함수이다. 반복 가능한 인수는 리스트, 튜플, 사전과 같이 처리하고자 하는 반복 가능한 객체이다. map 함수는 map 객체를 반환한다. map 객체는 결과를 지연 평가하는 반복기이다. map 객체를 리스트, 튜플, 집합으로 변환하여야 최종 결과를 얻을 수 있다.

예를 들어, 섭씨로 된 온도 목록이 있고 그것들을 화씨로 변환하고자 한다고 가정하자. 변환을 수행하는 함수를 정의하고 map 함수를 사용하여 목록에 적용할 수 있다.

```python
# Define a list of temperatures in Celsius
celsius = [0, 10, 20, 30, 40]
# Define a function that converts Celsius to Fahrenheit
def c_to_f(temp):
    return (temp * 9/5) + 32
# Use the map function to apply the function to the list
fahrenheit = map(c_to_f, celsius)
# Convert the map object to a list and print it
print(list(fahrenheit))
```

출력은 아래와 같다.

```
[32.0, 50.0, 68.0, 86.0, 104.0]
```

map 함수를 효과적이고 효율적으로 사용하는 방법에 대한 팁과 요령을 소개한다.

- lambda 함수를 사용하여 한 번만 필요한 익명 함수를 정의할 수 있다. 예를 들어, lambda 함수를 사용하여 이전 예제를 다음과 같이 다시 작성할 수 있다.

```python
# Define a list of temperatures in Celsius
celsius = [0, 10, 20, 30, 40]
# Use the map function with a lambda function to convert Celsius to Fahrenheit
fahrenheit = map(lambda temp: (temp * 9/5) + 32, celsius)
# Convert the map object to a list and print it
print(list(fahrenheit))
```

- 여러 번 반복할 수 있는 객체들의 길이가 같다면 map 함수의 인수로 사용할 수 있다. 함수 인수는 반복할 수 있는 인수의 수만큼의 인수를 사용해야 한다. 예를 들어 map 함수를 사용하여 두 리스트의 각 원소를 더할 수 있다.

```python
# Define two lists of numbers
list1 = [1, 2, 3, 4, 5]
list2 = [6, 7, 8, 9, 10]
# Use the map function with a lambda function to add the lists element-wise
list3 = map(lambda x, y: x + y, list1, list2)
# Convert the map object to a list and print it
print(list(list3))
```

출력은 아래와 같다.

```
[7, 9, 11, 13, 15]
```

- map 함수를 사용하여 사전의 키나 값에 함수를 적용할 수 있다. `dict.keys()` 또는 `dict.values()` 메서드를 사용하여 반복 가능한 키나 값을 얻은 다음 map 함수를 사용하여 이를 처리할 수 있다. 예를 들어 map 함수를 사용하여 사전의 키의 길이를 얻을 수 있다.

```python
# Define a dictionary of names and ages
names = {"Alice": 25, "Bob": 30, "Charlie": 35}
# Use the map function with a lambda function to get the lengths of the keys
lengths = map(lambda name: len(name), names.keys())
# Convert the map object to a list and print it
print(list(lengths))    # [5, 3, 6]
```

다음 절에서는 Python에서 filter 함수를 사용하여 조건을 만족하는 반복 가능한 요소를로 새로운 반복 가능한 객체를 반환하는 방법을 설명한다.

1. <a name="sec_04"></a> Python에서 filter 함수를 사용하는 방법
이 절에서는 Python에서 filter 함수를 사용하여 조건을 만족하는 요소만으로 새로운 반복 가능한 객체를 반환하는 방법을 설명한다. filter 함수를 효과적이고 효율적으로 사용하는 방법에 대한 팁과 요령도 알려줄 것이다.

filter 함수는 다음과 같은 구문을 갖는다.

```python
filter(function, iterable)
```

함수 인수는 반복 가능한 요소를 필터링하기 위한 조건으로 사용하려는 함수이다. 함수는 각 요소에 대한 부울 값(True 또는 False)을 반환해야 한다. 처리하려는 반복 가능한 인수는 리스트, 튜플 또는 사전과 같이 반복 가능한 객체이다. filter 함수는 결과를 지연 평가하는 반복기인 filter 객체를 반환한다. filter 객체를 목록, 튜플 또는 집합으로 변환하여 최종 결과를 얻을 수 있다.

예를 들어, 여러분이 이름 리스트를 가지고 있고 문자 'A'로 시작하는 이름들만으로 리스트를 구성하고자 한다고 가정하자. 이름이 'A'로 시작하는지 확인하는 함수를 정의하고 filter 함수를 사용하여 리스트에 적용할 수 있다.

```python
# Define a list of names
names = ["Alice", "Bob", "Charlie", "Anna", "David"]
# Define a function that checks if a name starts with 'A'
def starts_with_a(name):
    return name[0] == 'A'
# Use the filter function to apply the function to the list
filtered_names = filter(starts_with_a, names)
# Convert the filter object to a list and print it
print(list(filtered_names))
```

출력은 아래와 같다.

```
['Alice', 'Anna']
```

filter 함수를 효과적이고 효율적으로 사용하는 방법에 대한 팁과 요령은 다음과 같다.

- lambda 함수를 사용하여 한 번만 필요한 익명 함수를 정의할 수 있다. 예를 들어, lambda 함수를 사용하여 이전 예제를 다음과 같이 다시 작성할 수 있다.

```python
# Define a list of names
names = ["Alice", "Bob", "Charlie", "Anna", "David"]
# Use the filter function with a lambda function to filter out the names that start with 'A'
filtered_names = filter(lambda name: name[0] == 'A', names)
# Convert the filter object to a list and print it
print(list(filtered_names))
```

- filter 함수를 사용하여 사전의 키 또는 값을 필터링할 수 있다. `dict.items()` 메서드를 사용하여 키-값 쌍을 반복 가능하게 한 다음 filter 함수를 사용하여 이를 처리할 수 있다. 예를 들어, filter 함수를 사용하여 값이 30보다 큰 키-값 쌍을 필터링할 수 있다.

```python
# Define a dictionary of names and ages
names = {"Alice": 25, "Bob": 30, "Charlie": 35, "David": 40}
# Use the filter function with a lambda function to filter out the key-value pairs where the value is greater than 30
filtered_names = filter(lambda item: item[1] > 30, names.items())
# Convert the filter object to a list and print it
print(dict(filtered_names)) # {'Charlie': 35, 'David': 40}
```

- filter 함수를 사용하여 반복 가능한 객체에서 None, 거짓, 0 또는 비어 있는 요소를 필터링할 수 있다. 함수 인수로 bool 함수를 사용하면 참으로 간주되는 모든 값에 대해 True를, 거짓으로 간주되는 모든 값에 대해 False를 반환할 수 있다. 예를 들어 filter 함수를 사용하여 리스트에서 거짓 값을 제거할 수 있다.

```python
# Define a list of values
values = [1, 0, None, True, False, "", "Hello", [], [1, 2, 3]]
# Use the filter function with the bool function to remove the falsy values
filtered_values = filter(bool, values)
# Convert the filter object to a list and print it
print(list(filtered_values))    # [1, True, 'Hello', [1, 2, 3]]
```

다음 절에서는 Python에서 reduce 함수를 사용하여 함수를 반복 가능한 객체에 적용하여 단일 값으로 reduce하는 방법을 설명한다.

1. <a name="sec_05"></a> Python에서 reduce 함수를 사용하는 방법
이 절에서는 Python에서 reduce 함수를 사용하여 함수를 반복 가능한 객체에 적용하여 단일 값으로 reduce하는 방법을 설명할 것이다. reduce 함수를 효과적이고 효율적으로 사용하는 방법에 대한 팁과 요령도 알려줄 것이다.

reduce 함수는 다음과 같은 구문을 가진다.

```python
reduce(function, iterable)
```

함수 인수는 반복가능한 객체의 요소를 결합하는 데 사용하고자 하는 함수이다. 함수는 두 개의 인수를 사용하고 단일 값을 반환해야 한다. 반복가능한 인수는 리스트, 튜플, 사전과 같이 처리하고자 하는 반복가능한 객체이다. reduce 함수는 반복가능한 객체를 소진될 때까지 함수를 반복가능한 객체의 처음 두 요소에 적용한 후 결과와 다음 요소에 적용한 결과인 단일 값을 반환한다.

예를 들어, 여러분이 수들의 리스트를 가지고 있고 모든 수들의 합을 계산하고자 한다고 가정하자. 여러분은 두 수를 더하는 함수를 정의하고 그것을 리스트에 적용하기 위해 reduce 함수를 사용할 수 있다.

```python
# Import the reduce function from the functools module
from functools import reduce
# Define a list of numbers
numbers = [1, 2, 3, 4, 5]
# Define a function that adds two numbers
def add(x, y):
    return x + y
# Use the reduce function to apply the function to the list
sum = reduce(add, numbers)
# Print the result
print(sum)  # 15
```

출력은 아래와 같다.

```
15
```

reduce 함수를 효과적이고 효율적으로 사용하는 방법에 대한 팁과 요령은 다음과 같다.

- lambda 함수를 사용하여 한 번만 필요한 익명 함수를 정의할 수 있다. 예를 들어, lambda 함수를 사용하여 이전 예를 다시 작성할 수 있다.

```python
# Import the reduce function from the functools module
from functools import reduce
# Define a list of numbers
numbers = [1, 2, 3, 4, 5]
# Use the reduce function with a lambda function to add the numbers
sum = reduce(lambda x, y: x + y, numbers)
# Print the result
print(sum)
```

- reduce 함수를 사용하여 곱셈, 나눗셈, 뺄셈, 연결 등과 같은 반복 가능한 연산자에 임의의 이진 연산자를 적용할 수 있다. 연산자 모듈을 사용하여 이러한 연산자에 대해 미리 정의된 함수를 임포트할 수 있다. 예를 들어, reduce 함수를 사용하여 리스트에 있는 모든 수를 곱할 수 있다.

```python
# Import the reduce function from the functools module
from functools import reduce
# Import the mul function from the operator module
from operator import mul
# Define a list of numbers
numbers = [1, 2, 3, 4, 5]
# Use the reduce function with the mul function to multiply the numbers
product = reduce(mul, numbers)
# Print the result
print(product)
```

- reduce 함수를 사용하여 사전의 키나 값에 함수를 적용할 수 있다. `dict.keys()` 또는 `dict.values()` 메서드를 사용하여 반복할 수 있는 키나 값을 얻은 다음 reduce 함수를 사용하여 이를 처리할 수 있다. 예를 들어 reduce 함수를 사용하여 사전의 최대값을 얻을 수 있다.

```python
# Import the reduce function from the functools module
from functools import reduce
# Define a dictionary of names and ages
names = {"Alice": 25, "Bob": 30, "Charlie": 35, "David": 40}
# Use the reduce function with the max function to get the maximum value
max_age = reduce(max, names.values())
# Print the result
print(max_age)
```

다음 절에서는 반복 가능한 객체에 함수를 적용하는 다른 방법과 map, filter와 reduce 함수의 성능을 비교 평가하는 방법을 살펴볼 것이다.

1. <a name="sec_06"></a> Map, Filter와 Reduce 함수의 비교 및 성능
이 절에서는 Python에서 반복 가능한 객체에 함수를 적용하는 다른 방법과 map, filter와 reduce 함수의 성능을 비교 평가하는 방법을 설명한다. 또한 이러한 함수를 사용할 때와 피해야 할 때에 대한 모범 사례와 권장 사항도 알려줄 것이다.

Python에서 반복 가능한 객체에 함수를 적용하는 방법은 map, filter와 reduce뿐이 아니다. 동일한 결과를 얻을 수 있는 다른 방법들, 이를테면 리스트 컴플헨션, 생성자 표현식(generator expression), 루프 또는 내장 함수 등이 있다. 예를 들어 리스트 컴프리헨션를 사용하여 이전의 예들을 다음과 같이 다시 쓸 수 있다.

```python
# Define a list of temperatures in Celsius
celsius = [0, 10, 20, 30, 40]
# Use a list comprehension to convert Celsius to Fahrenheit
fahrenheit = [(temp * 9/5) + 32 for temp in celsius]
# Print the result
print(fahrenheit)
# Define a list of names
names = ["Alice", "Bob", "Charlie", "Anna", "David"]
# Use a list comprehension to filter out the names that start with 'A'
filtered_names = [name for name in names if name[0] == 'A']
# Print the result
print(filtered_names)
# Define a list of numbers
numbers = [1, 2, 3, 4, 5]
# Use a list comprehension to calculate the sum of the numbers
sum = sum([number for number in numbers])
# Print the result
print(sum)
```

출력은 아래와 같다.

```
[32.0, 50.0, 68.0, 86.0, 104.0]
['Alice', 'Anna']
15
```

그렇다면, 여러분은 어떤 방법을 사용할지를 어떻게 결정해야 할까? 여러분은 가독성, 효율성, 메모리 사용, 그리고 호환성과 같은 몇 가지 요소들을 고려해야 한다. 문제에 가장 적합한 방법을 선택하는 방법에 대한 일반적인 지침과 권장 사항은 다음과 같다.

- **가독성**: 일반적으로 코드를 더 잘 읽고 이해하기 쉽게 만드는 방법을 선택해야 한다. 가독성은 주관적이지만 몇 가지 일반적인 기준은 단순성, 명확성, 일관성, 우아함이다. 예를 들어, 많은 Python 프로그래머들은 map과 filter 함수보다 리스트 컴프리헨션를 더 간결하고 표현력이 있다고 생각하기 때문에 선호한다. 그러나 여러 번 반복할 필요가 있거나 lambda 함수로 쉽게 작성되지 않는 미리 정의된 함수를 사용해야 하는 경우와 같이 map과 filter 함수가 필요한 경우도 있다. reduce 함수는 종종 다른 방법보다 읽기가 덜한 것으로 간주되는데, 이는 혼란스럽고 따라하기 어렵기 때문이다. 따라서 reduce 함수를 아껴서 필요할 때만 사용해야 한다.
- **효율성**: 효율성은 코드가 얼마나 빨리 실행되고 어떤 작업을 완료하는 데 얼마나 많은 시간이 걸리는지를 나타낸다. 효율성은 반복 가능한 것의 크기와 타입, 함수의 복잡성과 타입, 방법의 구현과 최적화 등 여러 요소에 따라 달라진다. 일반적으로 문제에 대한 시간 복잡도가 가장 낮고 속도가 가장 빠른 방법을 선택해야 한다. `timeit` 모듈을 사용하여 다른 방법의 실행 시간을 측정하고 비교할 수 있다. 예를 들어, 다음 코드를 사용하여 섭씨를 화씨로 변환하는 map과 리스트 컴프리헨션의 실행 시간을 비교할 수 있다.

```python
# Import the timeit module
import timeit

# Define a list of temperatures in Celsius
celsius = [0, 10, 20, 30, 40]

# Define a function that converts Celsius to Fahrenheit
def c_to_f(temp):
    return (temp * 9/5) + 32

# Define a lambda function that converts Celsius to Fahrenheit
lambda_c_to_f = lambda temp: (temp * 9/5) + 32

# Define a list comprehension that converts Celsius to Fahrenheit
list_comp_c_to_f = [(temp * 9/5) + 32 for temp in celsius]

# Measure and compare the execution time of map and list comprehension
print(timeit.timeit("map(c_to_f, celsius)", globals=globals(), number=100000))
print(timeit.timeit("map(lambda_c_to_f, celsius)", globals=globals(), number=100000))
print(timeit.timeit("list_comp_c_to_f", globals=globals(), number=100000))
```

출력은 아래와 같다.

```
0.005304374964907765
0.00490233302116394
0.0006607919931411743
```

- **메모리 사용량**: 메모리 사용량은 코드가 메모리에서 차지하는 공간의 양과 실행 중에 메모리를 얼마나 할당하고 할당 해제하는지를 나타낸다. 메모리 사용량은 반복 가능한 객체의 크기와 타입, 함수의 복잡성과 유형, 방법의 구현과 최적화 등 여러 요소에 따라 달라진다. 일반적으로 문제에 대해 공간 복잡도가 가장 낮고 메모리 사용량이 가장 낮은 방법을 선택해야 한다. `sys` 모듈을 사용하여 여러 방법의 메모리 사용량을 측정하고 비교할 수 있다. 예를 들어, 다음 코드를 사용하여 섭씨를 화씨로 변환하는 map과 리스트 컴프리헨션을 비교할 수 있다.

```python
# Import the sys module
import sys

# Define a list of temperatures in Celsius
celsius = [0, 10, 20, 30, 40]

# Define a function that converts Celsius to Fahrenheit
def c_to_f(temp):
    return (temp * 9/5) + 32

# Define a lambda function that converts Celsius to Fahrenheit
lambda_c_to_f = lambda temp: (temp * 9/5) + 32

# Define a list comprehension that converts Celsius to Fahrenheit
list_comp_c_to_f = [(temp * 9/5) + 32 for temp in celsius]

# Measure and compare the memory usage of map and list comprehension
print(sys.getsizeof(map(c_to_f, celsius)))
print(sys.getsizeof(map(lambda_c_to_f, celsius)))
print(sys.getsizeof(list_comp_c_to_f))
```

출력은 아래와 같다.

```
8
48
120
```

- **호환성**: 호환성은 코드가 Python의 여러 버전과 구현, 여러 플랫폼과 환경에서 얼마나 잘 작동하는지를 나타낸다. 호환성은 방법의 가용성과 행동, 언어의 구문과 의미론, 플랫폼과 환경의 특징과 제한 등 여러 요소에 따라 달라집니다. 일반적으로 문제에 가장 호환되고 휴대하기 쉬운 방법을 선택해야 합니다. `__future__` 모듈을 사용하여 이전 버전의 파이썬에서는 사용할 수 없거나 다른 일부 기능을 사용할 수 있다. 6개의 모듈을 사용하여 Python 2와 Python 3 모두에서 작동하는 코드를 작성할 수도 있다. 예를 들어 Python 2에서 인쇄 기능과 나눗셈 연산자를 사용할 수 있도록 다음 코드를 사용할 수 있다.

```python
# Import the __future__ module to enable the print function and the division operator in Python 2
from __future__ import print_function, division

# Define a list of temperatures in Celsius
celsius = [0, 10, 20, 30, 40]

# Use a list comprehension to convert Celsius to Fahrenheit
fahrenheit = [(temp * 9/5) + 32 for temp in celsius]

# Print the result using the print function
print(fahrenheit)

# Define two numbers
x = 10
y = 3
# Divide the numbers using the division operator
z = x / y
# Print the result using the print function
print(z)
```

1. <a name="summary"></a> 요약
이 포스팅에서는 Python에서 map, filter, reduce 함수를 사용하는 방법을 배웠다. 이들은 어떤 함수를 리스트, 튜플, 사전과 같이 반복 가능한 객체에 적용하여, 수정된 요소들로 새로운 반복 가능한 객체 돌려줄 수 있는 내장된 함수이다. 또한 이 함수를 효과적이고 효율적으로 사용하는 방법, 반복 가능한 객체에 함수를 적용하는 다른 방법들과 성능을 비교하고 평가하는 방법에 대한 팁과 요령도 배웠다.

함수를 프로그램의 주요 구성 요소로 사용하는 것에 초점을 맞춘 프로그래밍 패러다임인 Python에서 함수형 프로그래밍의 예로는 map, filter와 reduce 함수를 들 수 있다. 함수형 프로그래밍은 테스트와 디버그가 더 쉬운 간결하고 우아하며 재사용 가능한 코드를 작성하는 데 도움을 줄 수 있다.

그러나 Python에서 반복 가능한 객체에 함수를 적용하는 방법은 map, filter, reduce 함수뿐이 아니다. 리스트 컴프리헨션, 생성자 표현식, 루프 또는 내장 함수 등과 같이 동일한 결과를 얻을 수 있는 다른 방법들도 있다. 여러분의 코드를 좀 더 읽기 쉽고, 효율적이며, 메모리에 절약하고, 당신의 문제에 적합하게 만드는 방법을 선택해야 한다.
