# Python Map, Filter와 Reduce 함수 <sup>[1](#footnote_1)</sup>

<font size="3">Python에서 반복 가능한 객체에 함수를 적용하기 위해 Map, Filter와 Reduce 함수를 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./map-filter-and-reduce-functions.md#intro)
1. [Map, Filter와 Reduce 함수란?](./map-filter-and-reduce-functions.md#sec_02)
1. [Python에서 map 함수를 사용하는 방법](./map-filter-and-reduce-functions.md#sec_03)
1. [Python에서 filter 함수를 사용하는 방법](./map-filter-and-reduce-functions.md#sec_04)
1. [Python에서 reduce 함수를 사용하는 방법](./map-filter-and-reduce-functions.md#sec_05)
1. [Map, Filter와 Reduce 함수의 비교와 성능](./map-filter-and-reduce-functions.md#sec_06)
1. [요약](./map-filter-and-reduce-functions.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 32 — Python Map, Filter, and Reduce Functions](https://python.plainenglish.io/python-tutorial-32-python-map-filter-and-reduce-functions-f1b1eea11fac)를 편역하였습니다.
